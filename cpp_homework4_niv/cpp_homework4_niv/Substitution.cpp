#include "PlainText.h"
#include "SubstitutionText.h"
#include <fstream>
#include <iostream>
#include <string>

SubstitutionText::SubstitutionText(std::string text, std::string dictionaryFileName):PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;
}

SubstitutionText::~SubstitutionText()
{

}

std::string SubstitutionText::encrypt()
{
	std::string key[26];
	std::string fileString = "";
	std::string encrypted = this->getText();
	int alphabetIndex = 0;
	int i = 0;

	std::ifstream openfile("dictionary.csv");

	if (openfile.is_open())
	{
		for (i=0;i<26;i++)//we check the whole file wich contains 26 characters
		{
			std::getline(openfile, fileString);
			key[i] = fileString;
		}
	}
	int offset = 0;
	for (i = 0; i < encrypted.length(); i++)
	{
		if (encrypted[i] <= 'z' && encrypted[i] >= 'a')
		{
			offset = encrypted[i] - 'a';//we find the index of the letter in the abc and in the file
			encrypted[i] = key[offset][2];
		}
	}
	this->isEncrypted = true;
	openfile.close();
	return encrypted;
}

std::string SubstitutionText::decrypt()
{
	std::string key[26];
	std::string fileString = "";
	std::string decrypted = this->getText();
	int alphabetIndex = 0;
	int i = 0;

	std::ifstream openfile("dictionary.csv");

	if (openfile.is_open())
	{
		for (i = 0; i < 26; i++)
		{
			std::getline(openfile, fileString);
			key[i] = fileString;
		}
	}
	int offset = 0;
	for (i = 0; i < decrypted.length(); i++)
	{
		offset = 0;//reseting offset every iteration
		if (decrypted[i] <= 'z' && decrypted[i] >= 'a')
		{
			for (int j = 0; j < 26 && decrypted[i] != key[offset][2]; j++)//until we found the letter we want to decrypt
			{
				offset++;//we find the index
			}
			decrypted[i] = key[offset][0];
		}
	}
	this->isEncrypted = true;
	openfile.close();
	return decrypted;
}

std::ostream& SubstitutionText::operator<<(std::ostream& out)
{
	out << "Substitution" << std::endl;
	out << this->encrypt() << std::endl;
	return out;
}
