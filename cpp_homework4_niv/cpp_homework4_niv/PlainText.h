#ifndef PLAINTEXT
#define PLAINTEXT

#include <iostream>

class PlainText
{
protected:
	std::string msg;
	bool isEncrypted;
	static int _NumOfTexts;

public:
	PlainText(std::string string);
	~PlainText();
	bool isEncryptedText();
	std::string getText();
	std::ostream& operator<<(std::ostream& out);
	int getNumOfTexts();
};

#endif // !PLAINTEXT

#pragma once
