#include "ShiftText.h"
#include <iostream>

ShiftText::ShiftText(std::string text, int key):PlainText(text)
{
	this->_key = key;
}

ShiftText::~ShiftText()
{
	
}

std::ostream& ShiftText::operator<<(std::ostream& out)
{
	out << "ShiftText" << std::endl;
	out << this->encrypt() << std::endl;

	return out;
}


std::string ShiftText::encrypt()
{

	int i = 0;
	char ch = ' ';
	std::string txt = this->getText();
	for (i = 0; txt[i] != '\0'; ++i)
	{
		ch = this->getText()[i];

		if (ch >= 'a' && ch <= 'z')//check if its an lowercase letter
		{
			ch = ch + this->_key;

			if (ch > 'z') {
				ch = ch - 'z' + 'a' - 1;
			}
			else if (128 - this->getText()[i] - this->_key <= 0)
			{
				int tempKey = -1 * (128 - this->getText()[i] - this->_key);
				ch = 'a' + tempKey;
			}

			txt[i] = ch;
		}

	}
	this->isEncrypted = true;
	return txt;
}

std::string ShiftText::decrypt()
{

	int i = 0;
	char ch = ' ';
	std::string txt = this->getText();
	for (i = 0; txt[i] != '\0'; ++i) {
		ch = this->getText()[i];

		if (ch >= 'a' && ch <= 'z') //check if the char is a lowercase letter, if no we will not  change this char
		{
			ch = ch - this->_key;

			if (ch < 'a')
			{
				ch = ch + 'z' - 'a' + 1;
			}

			txt[i] = ch;//set the encrypte char as an decrypt char
		}
	}
	this->isEncrypted = false;
	return txt;
}

/*std::string ShiftText::decrypt()
{
	int i = 0;
	char character = 'a';
	std::string text = this->msg;
	std::string decrypted = "";

	while (character != '\0')
	{
		character = text[i];

		if (character >= 'a' and character <= 'z')
		{
			character = character - this->_key;
			if (character < 'a')//if the character is not an english letter
			{
				character = character + 'z' - 'a' + 1;//we go from the start using how much its bigger than z ascii value
			}
		}
		decrypted += character;
		i++;
	}
	this->isEncrypted = false;
	return decrypted;
}*/