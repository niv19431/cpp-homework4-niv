#ifndef FILEHELPER
#define FILEHELPER

#include <iostream>

class FileHelper
{
public:
	static std::string readFileToString(std::string fileName);
	static void writeWordsToFile(std::string inputFileName, std::string outputFileName);
};

#endif // !FILEHELPER
#pragma once
