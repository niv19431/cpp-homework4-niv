#ifndef CAESARTEXT
#define CAESARTEXT

#include "ShiftText.h"
#include <iostream>

class CaesarText : public ShiftText
{
public:
	CaesarText(std::string string);
	~CaesarText();
	std::string encryptCaesar();
	std::string decryptCaesar();
	std::ostream& operator<<(std::ostream& out);
};

#endif // !CAESARTEXT

#pragma once