#include <iostream>
#include <fstream>
#include "BonusText.h"
#include "CaesarText.h"
#include "FileHelper.h"
#include "PlainText.h"
#include "ShiftText.h"
#include "SubstitutionText.h"

void pickOne()
{
	std::string text;
	std::string wayToCipher;
	std::cout << "Enter a string and the way to cipher it (Shift/Caesar/Subtitution): " << std::endl;
	std::cin >> text;
	std::cin >> wayToCipher;

	if (wayToCipher == "Shift")
	{
		int key = 0;
		std::cout << "Then enter a key : ";
		std::cin >> key;
		ShiftText shft(text,key);
		std::cout << shft.decrypt() << std::endl;
	}
	else if (wayToCipher == "Caesar")
	{
		CaesarText csr(text);
		std::cout << csr.decryptCaesar() << std::endl;
	}
	else if (wayToCipher == "Substitution")
	{
		std::string fileName;
		std::cout << "Enter a file name to read from : ";
		std::cin >> fileName;
		SubstitutionText sub(text,fileName);
		std::cout << sub.decrypt();
	}
}


void pickTwo()
{
	std::string fileName;

	std::cout << "Enter a File Name : ";
	std::cin >> fileName;

	std::string pick;
	std::cout << "what do you want to do Encrypt or Decrypt : ";
	std::cin >> pick;

	if (pick == "Encrypt")
	{
		FileHelper file;
		std::string encrypted = " ";
		std::string text;
		std::string encryptionType;
		text = file.readFileToString(fileName);

		std::cout << "Enter the way of encryption (Shift/Substitution/Caesar) : ";
		std::cin >> encryptionType;
		if (encryptionType == "Shift")
		{
			int key;
			std::cout << "Enter a Key : ";
			std::cin >> key;
			ShiftText shft(text,key);
			encrypted = shft.decrypt();
		}
		else if (encryptionType == "Substitution")
		{
			std::string readFile;
			std::cout << "Enter a file to read from : ";
			std::cin >> readFile;

			SubstitutionText sub(text, readFile);
			encrypted = sub.encrypt();
		}
		else if (encryptionType == "Caesar")
		{
			CaesarText csr(text);
			encrypted = csr.encryptCaesar();
		}
		else
		{
			std::cout << "Wrong way returning to menu";
		}
		if (encrypted != " ")
		{
			std::string option;
			std::cout << "What do you want to do print or save in a new file? (Print/Save)";
			std::cin >> option;
			if (option == "Print")
			{
				std::cout << encrypted;
			}
			else if (option == "Save")
			{
				std::string save;
				std::cout << "Enter the name of the save file : ";
				std::cin >> save;
				std::ofstream saveFile;
				saveFile.open(save);
				saveFile << encrypted;
				saveFile.close();
			}
		}
	}
	else if (pick == "Decrpyt")
	{
		FileHelper file;
		std::string decrypted = " ";
		std::string text;
		std::string decryptionType;
		text = file.readFileToString(fileName);

		std::cout << "Enter the way of decryption (Shift/Substitution/Caesar) : ";
		std::cin >> decryptionType;
		if (decryptionType == "Shift")
		{
			int key;
			std::cout << "Enter a Key : ";
			std::cin >> key;
			ShiftText shft(text, key);
			decrypted = shft.decrypt();
		}
		else if (decryptionType == "Substitution")
		{
			std::string readFile;
			std::cout << "Enter a file to read from : ";
			std::cin >> readFile;

			SubstitutionText sub(text, readFile);
			decrypted = sub.decrypt();
		}
		else if (decryptionType == "Caesar")
		{
			CaesarText csr(text);
			decrypted = csr.decryptCaesar();
		}
		else
		{
			std::cout << "Wrong way returning to menu";
		}
		if (decrypted != " ")
		{
			std::string option;
			std::cout << "What do you want to do print or save in a new file? (Print/Save)";
			std::cin >> option;
			if (option == "Print")
			{
				std::cout << decrypted;//printing
			}
			else if (option == "Save")
			{
				std::string save;
				std::cout << "Enter the name of the save file : ";
				std::cin >> save;
				std::ofstream saveFile;
				saveFile.open(save);
				saveFile << decrypted;//we enter the decrypted msg to the file
				saveFile.close();
			}
		}
	}
	else//if the user input is not expected
	{
		std::cout << "Wrong pick returning to the menu.";
	}
}

int main()
{
	int pick = 0;
	ShiftText temp("123312313", 6);
	while (pick != 4)
	{
		std::cout << "What do you want to do : \n1. decrypt text\n2. encrypt/decrypt from a file\n3. print num of texts\n4. leave";
		std::cin >> pick;
		switch (pick)
		{
		case 1:
			pickOne();
			break;

		case 2:
			pickTwo();
			break;

		case 3:
			std::cout << temp.getNumOfTexts() - 1 << std::endl;//we decrease by 1 because we created the temp
			break;

		case 4:
			std::cout << "bye o/";
			break;
		}
	}
}