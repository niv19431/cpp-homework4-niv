#include "FileHelper.h"
#include <iostream>
#include <fstream>
#include <string>

std::string FileHelper::readFileToString(std::string fileName)
{
	std::string string = "";
	std::string lineString = "";

	std::ifstream openfile(fileName);

	if (openfile.is_open())
	{
		while (!openfile.eof())
		{
			std::getline(openfile, lineString);
			string += lineString;
		}
	}
	return string;
}


void FileHelper::writeWordsToFile(std::string inputFileName, std::string outputFileName)
{
    std::ifstream readFile;
    std::ofstream writeFile;
    writeFile.open(outputFileName);
    readFile.open(inputFileName);
    std::string word;
    char x;
    word.clear();

    while (!readFile.eof())
    {
        x = readFile.get();

        while (x != ' ')
        {
            word = word + x;
            x = readFile.get();
            writeFile << x << std::endl;
        }
        std::cout << word << std::endl;
        word.clear();
    }

    readFile.close();
    writeFile.close();
}