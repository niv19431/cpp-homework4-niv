#ifndef SUBSTITUTIONTEXT
#define SUBSTITUTIONTEXT

#include "PlainText.h"
#include <iostream>

class SubstitutionText : public PlainText
{
private:
	std::string _dictionaryFileName;

public:
	SubstitutionText(std::string text,std::string fileName);
	~SubstitutionText();
	std::string encrypt();
	std::string decrypt();
	std::ostream& operator<<(std::ostream& out);
};
#endif // !SUBSTITUTIONTEXT

#pragma once
