#include "ShiftText.h"
#include "CaesarText.h"

CaesarText::CaesarText(std::string text):ShiftText(text,3)
{
	//nothing in here
}

CaesarText::~CaesarText()
{

}

std::string CaesarText::decryptCaesar()
{
	return this->decrypt();
}

std::string CaesarText::encryptCaesar()
{
	return this->encrypt();
}

std::ostream& CaesarText::operator<<(std::ostream& out)
{
	out << "Caesar" << std::endl;
	out << this->encrypt() << std::endl;
	return out;
}