#include "PlainText.h"
#include <iostream>

int PlainText::_NumOfTexts = 0;

PlainText::PlainText(std::string text)
{
	this->msg = text;
	this->isEncrypted = false;
	this->_NumOfTexts++;
}

PlainText::~PlainText()
{
	_NumOfTexts--;
}

bool PlainText::isEncryptedText()
{
	return this->isEncrypted;
}

std::string PlainText::getText()
{
	return this->msg;
}

int PlainText::getNumOfTexts()
{
	return this->_NumOfTexts;
}

std::ostream& PlainText::operator<<(std::ostream& out)
{
	out << this->getText() << std::endl;
	return out;
}