#ifndef SHIFTTEXT
#define SHIFTTEXT

#include "PlainText.h"

class ShiftText : public PlainText 
{
private:
	int _key;
	
public:
	ShiftText(std::string string, int key);
	~ShiftText();
	std::string decrypt();
	std::string encrypt();
	std::ostream& operator<<(std::ostream& out);
};


#endif // !SHIFTTEXT

#pragma once
